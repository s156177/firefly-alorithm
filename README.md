# Firefly alorithm

Realizacja algorytmu optymalizacji  

Algorytm świetlika (*ang. Firefly Algorithm*, w skrócie FA) jest nową meta-heurystyką rojową opracowaną w 2008 roku przez prof. Xin-SheYanga.  
Jest ona inspirowana zachowaniem społecznym świetlików i fenomenem ich bioluminescencyjnej komunikacji.  
W zamierzeniu algorytm jest dedykowany do zagadnień optymalizacji ciągłej z ograniczeniami, gdzie rozważany jest problem minimalizacji funkcji kosztu.  
## FA-podstawy działania    
Załóżmy że istnieje rój świetlików rozwiązujących iteracyjnie problem optymalizacji przy czym ***x<sub>i</sub>*** reprezentuje rozwiązanie dla świetlika i w iteracji ***k***, a ***f(x<sub>i</sub>)*** oznacza jego koszt.  

Każdy świetlik posiada wyróżniającą go atrakcyjność ***β*** która określa jak silnie przyciąga on innych członków roju.  

Jako atrakcyjność świetlika powinno się użyć malejącej funkcji odległości ***r<sub>j</sub>=d(x<sub>i</sub>,x<sub>j</sub>)*** do wybranego świetlika ***j***, np. jak sugeruje Yang funkcji:  

***β = β<sub>0</sub> e <sup>-γ r j <sup>2</sup></sup>***  

gdzie ***β<sub>0</sub>*** i ***γ***  są ustalonymi na wstępie parametrami algorytmu, odpowiednio: maksimum atrakcyjności i współczynnikiem absorpcji  

Każdy członek roju jest charakteryzowany przez świetlność ***I<sub>i</sub>*** , która może być bezpośrednio wyrażona jako odwrotność wartości funkcji kosztu ***f(x<sub>i</sub>)***   

Początkowo wszystkie świetliki są rozmieszczone w ***S*** (losowo bądź z użyciem pewnej deterministycznej strategii).  

By efektywnie eksplorować przestrzeń rozwiązań przyjmuje się że każdy świetlik *i* zmienia swoje położenie iteracyjnie, biorąc pod uwagę dwa czynniki:  
* atrakcyjność innych członków roju o większej świetlności ***I<sub>j</sub> > I<sub>i</sub>*** , ***∀<sub>j</sub>= 1, ...m, j≠i*** , która maleje wraz z odległością oraz 
* krok losowy ***u<sub>i</sub>*** .

Dla najjaśniejszego świetlika stosuje się jedynie wyżej wymieniony krok losowy.  

## Pseudokod
```
Begin
   1) Objective function: f ( x ) , x = ( x 1 , x 2 , . . . , x d ) ;
   2) Generate an initial population of fireflies x i ( i = 1 , 2 , … , n ) ;
   3) Formulate light intensity I so that it is associated with f ( x ) ;
      (for example, for maximization problems, I ∝ f ( x ) ;
   4) Define absorption coefficient γ
 
   While (t < MaxGeneration)
      for i = 1 : n (all n fireflies)
         for j = 1 : i (n fireflies)
            if ( I j > I i ),
               Vary attractiveness with distance r via exp ⁡ ( − γ r );
               move firefly i towards j;                
               Evaluate new solutions and update light intensity;
            end if 
         end for j
      end for i
      Rank fireflies and find the current best;
   end while

   Post-processing the results and visualization;

end
```



## Harmonogram projektu
### Etap 1:
* Implementacja podstawowej wersji algorytmu FA
* Testy

### Etap 2:
* Implementacja wybranych wariantów algorytmu FA
* Testy
* Porównanie z wersją podstawową

## Literatura:  
* Wikipedia https://en.wikipedia.org/wiki/Firefly_algorithm
* X. -S. Yang, Nature-Inspired Metaheuristic Algorithms, Luniver Press, 2008.
* Łukasik S. Algorytm świetlika - charakterystyka, właśności aplikacyjne i usprawnienia. http://home.agh.edu.pl/~slukasik/pub/021_Lukasik_KAEiOG2011(presentation).pdf
* Kułak L. Zastosowanie algorytmów metaheurystycznych do rozwiązywania zagadnień optymalizacyji globalnej. 


