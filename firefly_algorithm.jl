
# parametry algorytmu:

m = 100 # liczba swietlikow
max_generation = 100 # liczba iteracji
b0 = 1 # maksymalna atrakcyjnosc [0..1]
g = 1 # wspolczynnik absorbcji [0..10]
a = 1 # wspolczynnik losowosci [0..1]
u_min = -0.5 # dolna granica losowosci
u_max = 0.5 # gorna granica losowosci

# ograniczenia
min_x = -10
max_x = 10
min_y = -10
max_y = 10

# funkcja kosztu
function cost(x, y) 
    return (abs(x)+abs(y))*exp(-0.0625*(x^2+y^2))
end

function costVec(x) 
    return cost(x[1],x[2])
end

# funkcja odleglosci
function dist(x_i, x_j)
    return sqrt((x_i[1]-x_j[1])^2+(x_i[2]-x_j[2])^2)
end

# funkcja atrakcyjnosci
function beta(x_i, x_j)
    return b0*exp(-g*(dist(x_i, x_j))^2)
end

# generator losowych przesuniec
function displacement()
    return a*(rand(1)[1]-0.5), a*(rand(1)[1]-0.5)
end

# suma wektorow
function vecSum(x_i, x_j)
    return (x_i[1]+x_j[1], x_i[2]+x_j[2])
end

# wektor razy skalar
function vecMultScalar(x,s)
    return (s*x[1],s*x[2])
end

# przesuniecie x_i w strone x_j
function move(x_i,x_j)
    minus_x_i = vecMultScalar(x_i,-1)
    x_j_minus_x_i = vecSum(x_j,minus_x_i)
    b_i = beta(x_i,x_j)
    u_i = displacement()
    return vecSum(vecSum(x_i,vecMultScalar(x_j_minus_x_i,b_i)),u_i)
end

# generowanie swietlika w losowym miejscu (zgodnie z ograniczeniami)
function randomFirefly()
    spread_x = max_x - min_x;
    spread_y = max_y - min_y;
    rand_x = rand(1)[1]*spread_x - spread_x / 2
    rand_y = rand(1)[1]*spread_y - spread_y / 2
    return (rand_x, rand_y)
end

# ze zbioru swietlikow wybiera tego o najlepszym wyniku
function bestFireflyIndex(fireflies)
    best_value = -10000000000;
    best_index = 0;
    for f in 1:m
        if (costVec(fireflies[f]) > best_value)
            best_index = f
            best_value = costVec(fireflies[f])
        end
    end
    return best_index
end

# faktyczny algorytm
fireflies = fill((0.0, 0.0), m)
for f in 1:m
    fireflies[f] = randomFirefly()
end

for k in 1:max_generation
    for i in 1:m
        for j in 1:m
            if (i != j)
                if (costVec(fireflies[j]) > costVec(fireflies[i]))
                    fireflies[i] = move(fireflies[i],fireflies[j])
                end
            end
        end
    end
    bestIndex = bestFireflyIndex(fireflies)
    fireflies[bestIndex] = vecSum(fireflies[bestIndex],displacement())
end

# wynik
fireflies[bestFirefly(fireflies)]


