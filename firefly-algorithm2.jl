
# parametry algorytmu:

m = 100 # liczba swietlikow
max_generation = 100 # liczba iteracji
b0 = 1 # maksymalna atrakcyjnosc [0..1]
g = 1 # wspolczynnik absorbcji [0..10]
a = 0.1 # wspolczynnik losowosci [0..1]
u_min = -0.5 # dolna granica losowosci
u_max = 0.5 # gorna granica losowosci

# funkcja odleglosci
function dist(x_i, x_j)
    return sqrt((x_i[1]-x_j[1])^2+(x_i[2]-x_j[2])^2)
end

# funkcja atrakcyjnosci
function beta(x_i, x_j)
    return b0*exp(-g*(dist(x_i, x_j))^2)
end

# generator losowych przesuniec
function displacement()
    spread = Int(u_max - u_min)
    return a*(rand(spread)[1]+u_min), a*(rand(spread)[1]+u_min)
end

# przesuniecie x_i w strone x_j
function move(x_i,x_j)
    b_i = beta(x_i,x_j)
    u_i = displacement()
    return x_i .+ ((x_j .- x_i) .* b_i) .+ u_i
end

# generowanie swietlika w losowym miejscu (zgodnie z ograniczeniami)
function randomFirefly(limits)
    spread_x = limits[1][2] - limits[1][1]
    spread_y = limits[2][2] - limits[2][1]
    rand_x = rand(1)[1]*spread_x - spread_x / 2
    rand_y = rand(1)[1]*spread_y - spread_y / 2
    return (rand_x, rand_y)
end

# ze zbioru swietlikow wybiera tego o najlepszym wyniku
function bestFireflyIndex(fireflies, costFunction)
    best_value = -10000000000;
    best_index = 0;
    for f in 1:m
        if (costFunction(fireflies[f]) > best_value)
            best_index = f
            best_value = costFunction(fireflies[f])
        end
    end
    return best_index
end

# faktyczny algorytm
function fireflyAlgorythm(costFunction, limits)
    fireflies = fill((0.0, 0.0), m)    
    for f in 1:m
        fireflies[f] = randomFirefly(limits)
    end
    for k in 1:max_generation
        for i in 1:m
            for j in 1:m
                if (i != j)
                    if (costFunction(fireflies[j]) > costFunction(fireflies[i]))
                        fireflies[i] = move(fireflies[i],fireflies[j])
                    end
                end
            end
        end
        bestIndex = bestFireflyIndex(fireflies, costFunction)
        fireflies[bestIndex] = fireflies[bestIndex] .+ displacement()
    end
    return fireflies[bestFireflyIndex(fireflies, costFunction)]
end

function testFunction1(x) 
    return (abs(x[1])+abs(x[2]))*exp(-0.0625*(x[1]^2+x[2]^2))
end

#Beale function
function testFunction2(x)
    -((1.5-x[1]+x[1]*x[2])^2+(2.25-x[1]+x[1]*x[2]^2)^2+(2.625-x[1]+x[1]*x[2]^3)^2)
end

# Himmelblau's function
function testFunction3(x)
    -((x[1]^2+x[2]-11)^2+(x[1]+x[2]^2-7)^2)
end

fireflyAlgorythm(testFunction3, ((-4.5,4.5),(-4.5,4.5)))
